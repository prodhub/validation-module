#!/usr/bin/env bash

function node_publish {
	npm i -g npm-cli-login
	npm-cli-login
	npm publish --access public
	echo "Published..."
}
npm i json -g

n=0
packageName=$(cat package.json | json name)

vREMOTE=$(npm show ${packageName} version)
vJSON=$(cat package.json | json version)
vCOMMIT=$(git log --format=%B -n 1 --skip=${n} | grep -Eo "[0-9]+\.[0-9]+\.[0-9]+")


echo packageName: ${packageName}
echo vRemote: ${vREMOTE}
echo vJson: ${vJSON}
echo vCommit: ${vCOMMIT}

until [ "$vCOMMIT" = "$vJSON" ] || [[ ${n} -gt 50 ]]; do

	versionTagFound=$(git log --format=%B -n 1 --skip=${n} | grep -Eo "[0-9]+\.[0-9]+\.[0-9]+" | wc -c)
	echo "iteration" ${n} ":"

	if [ ${versionTagFound} -eq 0 ]
		then
			echo "Could't find version in commit message"
		else
			echo ${vCOMMIT}
	fi

	let n+=1
	vCOMMIT=$(git log --format=%B -n 1 --skip=${n} | grep -Eo "[0-9]+\.[0-9]+\.[0-9]+")
done

echo "Comparing versions:" ${vREMOTE} "vs" ${vCOMMIT}

if [ "$vREMOTE" != "$vCOMMIT" ]
	then
	  echo "OK! Uploading commit" ${vCOMMIT} "with JSON" ${vJSON} ", updating old remote version" ${vREMOTE}
	  node_publish
	else
	  echo "You should put a version upd commit (npm version ...) to have new version published to npmjs.com"
fi



