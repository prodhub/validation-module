# Validation

Form validation module


## Install

```sh
$ npm install --save @adwatch/validation
```


## Usage

```js
import Validation from '@adwatch/validation';
// or
var moduleName = require('@adwatch/validation/build');

///
///...
///
```


## API

### moduleName.init(options)

Blablabla blabla bla.

### moduleName.blabla(options)

Blablabla blabla bla.

### moduleName.foo(bar)

Blablabla blabla bla.

#### options.status

Type: `boolean`  
Default: `false` *(a few information about this prop)*

A quite interesting description.  

## License

MIT © 
