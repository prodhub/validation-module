//Protect
var _protect = {
	passBuffer: '',
	inputTemplate: {
		oC: /[а-яё]|\s/gi,
		oL: /[a-z]|\s/gi,
		oN: /\d|\s/g,
		oS: /<\/?[a-z0-9]+>.*/gi,
		email: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
		hasUppercase: /[A-Z]/,
		hasLowercase: /[a-z]/,
		hasDigits: /[0-9]/,
		hasSpecials: /[_\W]/,
		hasDigitsOnly: /^[0-9]+$/,
		hasLettersOnly: /^[a-zA-Z]+$/
	},
	//Apply password cases
	passScore: function(val, minPassLength){
		let power = 0,
			result = {};

		result.length = val.length;

		result.analysis = {
			hasUppercase: this.inputTemplate.hasUppercase.test(val),
			hasLowercase: this.inputTemplate.hasLowercase.test(val),
			hasDigits: this.inputTemplate.hasDigits.test(val),
			hasSpecials: this.inputTemplate.hasSpecials.test(val)
		};

		power = this.setScorePassword(val, result.analysis, result.length);
		result.power = power;

		return result;
	},
	//Set password score
	setScorePassword: function(password, analyse, length){
		let score = 0,
			variationCount = 0,
			letters = {},
			i = 0;

		if(!password){
			return score;
		}

		for(i; i < length; i++){
			letters[password[i]] = (letters[password[i]] || 0) + 1;

			score += 5.0 / letters[password[i]];
		}

		for(var check in analyse){
			variationCount += analyse[check] ? 1 : 0;
		}
		score += (variationCount - 1) * 10;

		return parseInt(score);
	},
	//Apply entry filter
	onFilter: function(e, elem){
		let symbol = String.fromCharCode(e.keyCode),
			tempRaw = !(elem.data('f') === undefined) ? "" + elem.data('f') : '',
			temp = tempRaw.split('_'),
			tempCase = temp[0] || 'default',
			tempCount = +temp[1]-1 || 100,
			valCount = elem.val().length;
		if(e.type == 'change') {
			let str = $(elem).val(),
				strLength = str.length,
				i = 0;

			for(i; i < strLength; i++ ){
				if(!(str[i].match(_protect.inputTemplate[tempCase]))){
					return false;
				}
			}

			return true;
		}

		if(valCount > tempCount){
			if(tempCase !== 'default'){
				return false;
			}
			return 'stop';
		}

		if(tempCase == _protect.inputTemplate.oL && !symbol.match(_protect.inputTemplate[tempCase])){
			return false;
		}

		if(symbol.match(_protect.inputTemplate.oS)){
			return false;
		}

		if(!symbol.match(_protect.inputTemplate[tempCase])){
			return false;
		}else if(tempCase == 'oE' || tempCase == _protect.inputTemplate.oL){
			var code = (document.all) ? e.keyCode : e.charCode;
			if(!((code < 1040) || (code > 1104))) return false;
		}

		return true;
	},
	setServerError: function(that, xhr){
		var errors = JSON.parse(xhr.error().responseText).fields;
		var message = JSON.parse(xhr.error().responseText).message;

		if(errors.length){
			errors.forEach(function(item, i){

				let formGroup = that.$blockForm.find('[name="' + item.name + '"]').closest(that.setErrors.targetParent);
				formGroup.addClass(that.clsErrorInput);
				formGroup.find(that.setErrors.targetError).text(item.message);

			});
		}
	},
	setErrorsTo: function(that){
		if(_protect.stateBuffer.length){
			let inputs = _protect.stateBuffer,
				parentItem = that.setErrors.targetParent || '',
				elemErrorMsg = that.setErrors.targetError || '';

			inputs.forEach(function(item){
				let $parrentItem = (parentItem) ? $(item.elem.closest(parentItem)) : '',
					$elemErrorMsg = ($parrentItem) ? $parrentItem.find(elemErrorMsg) : '';

				if(item.validation === 'error'){
					if(parentItem){
						$parrentItem.addClass(that.clsErrorInput);
						if(elemErrorMsg){
							$elemErrorMsg.text(that.ERRORS_MSG[item.type][item.status]);
						}
					}else{
						$(item.elem).addClass(that.clsErrorInput);
					}
				}else if(item.validation === 'success'){
					if(parentItem){
						$(item.elem.closest(parentItem)).addClass(that.clsSuccessInput);
						$elemErrorMsg.text('');
					}else{
						$(item.elem).addClass(that.clsSuccessInput);
					}
				}
			});
		}
	},
	validationCase: function(that, elem, val, type){
		let result = {}, valid, status;

		switch (type){
			case 'required' || "" :
			{
				let data = (val) ? ('' + val).trim() : '';
				setDecision((!!data), true);
				break;
			}
			case 'select':
			{
				setDecision(!($(elem).find('option:selected').val() == $(elem).find('option:eq(0)').val()), true);
				break;
			}
			case 'email':
			{
				setDecision(val, _protect.inputTemplate.email.test(val));
				break;
			}
			case 'password':
			{
				let passScore = _protect.passScore(val, that.minPassLength);
				setDecision(val, (passScore.analysis.hasLowercase && passScore.analysis.hasDigits && passScore.length));
				_protect.passBuffer = val;
				return {elem: elem, val: val, type: type, status: status, validation: valid, score: (passScore) ? passScore.power : 0};
			}
			case 'equal':
			{
				setDecision(val, (_protect.passBuffer === val));
				break;
			}
			case 'keyError':
			{
				setDecision('keyError', $(elem).data('f'));
				break;
			}
		}

		function setDecision(val, condition){
			if(val === 'keyError'){
				valid = 'error';
				status = condition;
				return false;
			}
			if(val){
				if(condition){
					valid = 'success';
					status = 'success';
				}else{
					valid = 'error';
					status = 'invalid';
				}
			}else{
				valid = 'error';
				status = 'empty';
			}
		}

		return {elem: elem, val: val, type: type, status: status, validation: valid};
	}
};

function _controller(){
	let that = this;

	that.$blockForm.on('submit', (e)=>{
		if(that.ajaxBody){
			e.preventDefault();

			that.clearCls();
			that.check();

			if(_protect.formState === that.clsSuccessForm){
				that.send();
			}
		}
	});

	if(that.checkPassScore){
		that.$blockForm.find(that.checkPassScore.target).on('keyup', function(){
			that.checkPassScore.callback(_protect.passScore($(this).val(), that.minPassLength).power);
		});
	}

	if(that.validByChange){
		that.clearCls();
		that.$groupInputs.on('change', that.check.bind(that));
	}

	if(that.validByKeypress){
		var checkKey = function (obj){
			let {current = '', keyError = false, keyVal = false} = obj;
			let keyOption = (that.validByKeypress === 'current') ? current
				: (that.validByKeypress === 'parallel') ? false : false;
			let resOption = {current: keyOption};
			if(keyError){resOption.keyError = keyError}
			if(keyVal){resOption.keyVal = keyVal}

			that.check(resOption);
		};
		if(!that.filters){
			that.$groupInputs.on('keyup change', function(){
				checkKey({current: $(this)});
			});
		}
	}

	that.$groupInputs.on('keypress change', function(e){
		if(that.filters){
			if(_protect.onFilter(e, $(this)) == 'stop'){
				checkKey({current: $(this), keyError: 'keyError'});
				return false;
			}else if(!(_protect.onFilter(e, $(this)))){
				$(this).val('');
				checkKey({current: $(this), keyError: 'keyError'});
				return false;
			}else{
				let cleanRes = {current: $(this), keyError: ''};
				if(e.type === 'keypress'){cleanRes.keyVal = e.key.trim()}
				checkKey(cleanRes);
			}
		}
	});
	that.$groupInputs.on('keyup', function(e){
		if(that.filters){
			if($(this).val().match(_protect.inputTemplate.oS)){
				$(this).val('');
			}
		}
	});
}

class Validation {
	constructor(config){
		if(config){
			//ui - elems
			this.$body = $('body');
			this.$html = $('html');
			this.$window = $('window');
			this.$blockForm = $(config.$blockForm);
			this.$blockParentForm = $(config.$blockForm).parent();
			this.$groupInputs = $(config.$blockForm).find('[data-validation]');
			this.$groupInputsAll = $(config.$blockForm).find('input, select, textarea');
			this.$elemSubmit = $(config.$blockForm).find('[type="submit"]');
			//ui - cls
			this.clsErrorForm = (typeof config.clsErrorForm === 'string') ? config.clsErrorForm : 'error';
			this.clsSuccessForm = (typeof config.clsSuccessForm === 'string') ? config.clsSuccessForm : 'success';
			this.clsErrorInput = (typeof config.clsErrorInput === 'string') ? config.clsErrorInput : 'error';
			this.clsSuccessInput = (typeof config.clsSuccessInput === 'string') ? config.clsSuccessInput : '';
			//callbacks
			this.callBeforeValidation = (typeof config.callBeforeValidation === 'function') ? config.callBeforeValidation : '';
			this.callAfterValidation = (typeof config.callAfterValidation === 'function') ? config.callAfterValidation : '';
			this.callErrorValidation = (typeof config.callErrorValidation === 'function') ? config.callErrorValidation : '';
			this.callSuccessValidation = (typeof config.callSuccessValidation === 'function') ? config.callSuccessValidation : '';
			//options
			this.blocked = (typeof config.blocked === 'boolean') ? config.blocked : false;
			this.minPassLength = (isFinite(config.minPassLength)) ? config.minPassLength : 6;
			this.scrollToError = (typeof config.scrollToError === 'object') ? config.scrollToError : false;
			this.clearInputs = (typeof config.clearInputs === 'boolean') ? config.clearInputs : false;
			this.validByChange = (typeof config.validByChange === 'boolean') ? config.validByChange : false;
			this.validByKeypress = (typeof config.validByKeypress === 'string') ? config.validByKeypress : false;
			this.filters = (typeof config.filters === 'boolean') ? config.filters : false;
			this.setErrors = (typeof config.setErrors === 'object') ? config.setErrors : false;
			this.checkPassScore = (typeof config.checkPassScore === 'object') ? config.checkPassScore : false;
			//AJAX
			this.ajaxBody = (typeof config.ajaxBody === 'object') ? config.ajaxBody : false;
			this.callbackAjaxSuccess = (typeof config.callbackAjaxSuccess === 'function') ? config.callbackAjaxSuccess : '';
			this.callbackAjaxComplete = (typeof config.callbackAjaxComplete === 'function') ? config.callbackAjaxComplete : '';
			this.callbackAjaxError = (typeof config.callbackAjaxError === 'function') ? config.callbackAjaxError : '';


			this.ERRORS_MSG = (typeof config.ERRORS_MSG === 'object') ? config.ERRORS_MSG : {
				empty: ((config.ERRORS_MSG) ? config.ERRORS_MSG.empty : '') ? config.ERRORS_MSG.empty : 'Значение не должно быть пустым',
				invalid: ((config.ERRORS_MSG) ? config.ERRORS_MSG.invalid : '') ? config.ERRORS_MSG.invalid : 'Значение не корректно',
				keyError:{
					oC: ((config.ERRORS_MSG) ? config.ERRORS_MSG.keyError.oC : '') ? config.ERRORS_MSG.keyError.oC : 'Разрешены только кириллические символы',
					oL: ((config.ERRORS_MSG) ? config.ERRORS_MSG.keyError.oL : '') ? config.ERRORS_MSG.keyError.oL : 'Разрешены только латинские символы',
					oN: ((config.ERRORS_MSG) ? config.ERRORS_MSG.keyError.oN : '') ? config.ERRORS_MSG.keyError.oN: 'Разрешены только цифры',
					oE: ((config.ERRORS_MSG) ? config.ERRORS_MSG.keyError.oE : '') ? config.ERRORS_MSG.keyError.oE: 'Разрешены только цифры, спецсимволы и символы латинского алфавита',
				},
				required:{
					empty: ((config.ERRORS_MSG) ? config.ERRORS_MSG.required.empty : '') ? config.ERRORS_MSG.required.empty : 'Значение не должно быть пустым'
				},
				select:{
					empty: ((config.ERRORS_MSG) ? config.ERRORS_MSG.select.empty : '') ? config.ERRORS_MSG.select.empty : 'Значение не должно быть пустым'
				},
				email:{
					empty: ((config.ERRORS_MSG) ? config.ERRORS_MSG.email.empty : '') ? config.ERRORS_MSG.email.empty : 'Значение не должно быть пустым',
					invalid: ((config.ERRORS_MSG) ? config.ERRORS_MSG.email.invalid : '') ? config.ERRORS_MSG.email.invalid : 'Неправильный формат'
				},
				password:{
					empty: ((config.ERRORS_MSG) ? config.ERRORS_MSG.password.empty : '') ? config.ERRORS_MSG.password.empty : 'Значение не должно быть пустым',
					invalid: ((config.ERRORS_MSG) ? config.ERRORS_MSG.password.invalid : '') ? config.ERRORS_MSG.password.invalid : 'Пароль Слишком простой'
				},
				equal:{
					empty: ((config.ERRORS_MSG) ? config.ERRORS_MSG.equal.empty : '') ? config.ERRORS_MSG.equal.empty : 'Значение не должно быть пустым',
					invalid: ((config.ERRORS_MSG) ? config.ERRORS_MSG.equal.invalid : '') ? config.ERRORS_MSG.equal.invalid : 'Значения не совпадают'
				}
			};
		} else {
			throw new TypeError('Config object is not defined!');
		}

	}

	//Check data validation
	check({
		callBeforeValidation = this.callBeforeValidation,
		callAfterValidation = this.callAfterValidation,
		callErrorValidation = this.callErrorValidation,
		callSuccessValidation = this.callSuccessValidation,
		current = current || false,
		keyError,
		keyVal
	} = {}){
		if(!this.blocked){
			let that = this,
				groupInputs = current || that.$groupInputs,
				inputLength = groupInputs.length;

			if(callBeforeValidation){callBeforeValidation()}

			if(inputLength){
				let i = 0,
					validationPack = [];
				for(i; inputLength > i; i++){
					let val = keyVal || ((groupInputs[i].type === 'checkbox') ? groupInputs[i].checked : (groupInputs[i].type === 'radio') ? radioParse(groupInputs[i].name) : groupInputs[i].value),
						type = keyError || ((groupInputs[i].getAttribute('data-validation')) ? groupInputs[i].getAttribute('data-validation') : 'required'),
						equalFlag = false;

					if(!validationPack.length){
						validationPack.push(_protect.validationCase(that, groupInputs[i], val, type));
					}else{
						for(let j = 0; validationPack.length > j; j++){
							if(validationPack[j].elem.name === groupInputs[i].name){
								equalFlag = true;
							}
						}
						if(!equalFlag){
							validationPack.push(_protect.validationCase(that, groupInputs[i], val, type));
						}
					}
				}

				for(let t = 0; validationPack.length > t; t++){
					if(validationPack[t].validation === 'error'){
						if(that.scrollToError){
							let offsetElem = $(validationPack[t].elem).offset().top,
								negativeOffset = that.scrollToError.offsetTop || 0,
								duration = that.scrollToError.duration || 300;

							if(offsetElem){
								that.$body.animate({scrollTop: offsetElem - negativeOffset}, duration, 'linear');
								that.$html.animate({scrollTop: offsetElem - negativeOffset}, duration, 'linear');
							}
						}
						_protect.formState = that.clsErrorForm;
						if(that.clsErrorForm){
							that.$blockForm.addClass(that.clsErrorForm);
						}
						if(callErrorValidation){callErrorValidation()}
						break;
					}
					if(validationPack.length-1 === t){
						_protect.formState = that.clsSuccessForm;
						if(that.clsSuccessForm){
							that.$blockForm.addClass(that.clsSuccessForm);
						}
						if(that.clearInputs){this.clearForm()}
						if(callSuccessValidation){callSuccessValidation()}
					}
				}

				_protect.stateBuffer = validationPack;
				if(callAfterValidation){callAfterValidation()}
				if(that.setErrors){_protect.setErrorsTo(that)}
				return this;
			}

			function radioParse(name){
				let i = 0;
				for(i; inputLength > i; i++){
					if(groupInputs[i].name === name){
						if(groupInputs[i].checked){
							return true;
						}
					}
				}
				return false;
			}
		}
	}

	//Clear from inputs
	clearForm(){
		this.$blockForm[0].reset();

		return this;
	}


	//Clear error/success classes
	clearCls(){
		let that = this;

		that.$blockForm.removeClass(`${that.clsErrorForm} ${that.clsSuccessForm}`);
		if(that.setErrors){
			if(that.setErrors.targetParent){
				that.$blockForm.find(that.setErrors.targetParent).removeClass(`${that.clsErrorInput} ${that.clsSuccessInput}`);
			}
			that.$blockForm.find('[data-validation]').removeClass(`${that.clsErrorInput} ${that.clsSuccessInput}`);
		}

		return this;
	}

	//Send Form
	send({
		newAjaxBody = this.ajaxBody,
		callbackAjaxComplete = this.callbackAjaxComplete,
		callbackAjaxSuccess = this.callbackAjaxSuccess,
		callbackAjaxError = this.callbackAjaxError,
	} = {}){
		let that = this;

		if(newAjaxBody){
			let type = (newAjaxBody.type === 'method') ? that.$blockForm.attr('method') : newAjaxBody.type,
				url = (newAjaxBody.url === 'action') ? that.$blockForm.attr('action') : newAjaxBody.url,
				data = (newAjaxBody.data === 'serialize') ? that.$blockForm.serialize() : newAjaxBody.data,
				body = {type: type, url: url, data: data};

			that.$elemSubmit.prop('disabled', true);

			$.ajax(body).done((data) => {
				if(callbackAjaxSuccess){callbackAjaxSuccess()}
				if(that.clearInputs){this.clearForm()}
			}).fail((xhr, textStatus) => {
				if(callbackAjaxError){callbackAjaxError()}
			}).complete((xhr, textStatus) => {
				that.xhr = xhr;
				that.textStatus = textStatus;
				if(textStatus == 'error'){_protect.setServerError(that, that.xhr)}
				if(callbackAjaxComplete){callbackAjaxComplete()}
				that.$elemSubmit.prop('disabled', false);
			});

			return this;
		}

		that.$blockForm.submit();
	}

	//Disable all input
	disable(selectors){
		let that = this;

		if(selectors){
			selectors.forEach(function(item){
				$(item).attr('disabled', true);
			});
		}else{
			that.$groupInputsAll.attr('disabled', true);
		}

		return this;
	}

	//Enable all input
	enable(selectors){
		let that = this;

		if(selectors){
			selectors.forEach(function(item){
				$(item).attr('disabled', false);
			});
		}else{
			that.$groupInputsAll.attr('disabled', false);
		}
		return this;
	}

	//Detach form
	detach(){
		if(this.$blockForm.is(':visible')){
			this.$blockForm.detach();
		}else{
			this.$blockForm.appendTo(this.$blockParentForm);
		}
		return this;
	}

	get isBlocked(){
		return this.blocked;
	}

	set isBlocked(val){
		if(typeof val === 'boolean'){this.blocked = val}
	}

	//Get state validation
	get getStateValidation(){
		return _protect.stateBuffer;
	}

	//Create new Form
	static create(options){
		$(options.view.target).append(options.view.template);
		return new Validation(options.config);
	}

	controller(){
		_controller.call(this);
		return this;
	}

	init(){
		this.controller();
		return this;
	}
}

module.exports = Validation;